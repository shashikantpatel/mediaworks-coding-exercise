export interface Registration {
  id: string;
  firstName: string;
  lastName: string;
  state: string;
  email: string;
  subscribe: boolean;
}
