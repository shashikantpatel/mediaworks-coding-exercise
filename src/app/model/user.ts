
export interface User {
  id: string;
  password: string;
  token: string;
}
