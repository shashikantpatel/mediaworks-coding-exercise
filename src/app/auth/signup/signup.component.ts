import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EmailMatch } from 'src/app/validators/email-match.validator';
import { AuthService } from 'src/app/services/auth.service';
import { Registration } from 'src/app/model/registration';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  registerForm: FormGroup;
  submitted = false;
  showForm = true;

  constructor(private formBuilder: FormBuilder,
    private authService: AuthService) { }

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      state: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      confirmEmail: ['', Validators.required, Validators.email],
      subscribe: []
    }, {
      validator: EmailMatch('email', 'confirmEmail')
    });
  }

  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.registerForm.invalid) {
      return;
    }

    this.authService.register(this.registerForm.value)
      .subscribe(
        data => {
          if (data as Registration) {
            this.showForm = false;
            //alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value, null, 4));
          }
        },
        () => {
          this.showForm = true;
        });
  }

}
