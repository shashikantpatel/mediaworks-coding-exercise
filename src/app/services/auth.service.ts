import { Injectable } from '@angular/core';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Registration } from '../model/registration';
import { environment } from 'src/environments/environment';
import { User } from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public user: Observable<any>;
  userSubject: any;

  constructor(
    private router: Router,
    private http: HttpClient
  ) {
    this.userSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('user')));
    this.user = this.userSubject.asObservable();
  }

  public get userValue(): User {
    return this.userSubject.value;
  }

  login(value: string): Observable<any> {
    // create a temp user to compare password this can be wired to a api endpoint
    let user = {
      id: 1,
      password: 'test',
      token: 'token-go'
    }

    // if password doesn't match return empty observable
    if(user.password !== value)
    {
      return of({});
    }

    localStorage.setItem('user', JSON.stringify(user));
    this.userSubject.next(user);

    return of(user);
  }

  logout() {
    localStorage.removeItem('user');
    this.userSubject.next(null);
    this.router.navigate(['/auth/login']);
  }

  register(registration: Registration) {
    return this.http.post(`${environment.apiUrl}/registrations?key=${registration.email}`, registration);
  }
}


