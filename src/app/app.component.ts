import { Component } from '@angular/core';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'mediaworks-coding-exercise';

  user: any;

  constructor(private authService: AuthService) {
    this.authService.user.subscribe(x => this.user = x);
}

logout() {
    this.authService.logout();
}

}
